from django.urls import path
from accounts.views import account_list

from accounts.views import login_view, logout_view, signup_view

urlpatterns = [
    #path("", receipt_list, name="home"),
    path("signup/", signup_view, name="signup"),
    path("logout/", logout_view, name="logout"),
    path("login/", login_view, name="login"),
    path("accounts", account_list, name="account_list"),
]
