from django.db import models
from django.conf import settings

class Account(models.Model):
    name = models.CharField(max_length=150)
    bakance = models.DecimalField(max_digits=10, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
