from django.shortcuts import render, redirect
from accounts.models import Account
from django.contrib.auth import authenticate, login, logout
#from django.contrib.auth.forms import SignupForm
from .forms import LoginForm, SignupForm
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignupForm


def account_list(request):
    accounts = Account.objects.all()

    return render(request, 'accounts/account_list.html')

def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
            else:
                form.add_error(None, "Invalid username or password.")
    else:
        form = LoginForm()

    return render(request, "login.html", {"form": form})

#create function that logs a person out then redirects to "login"
def logout_view(request):
    logout(request)
    return redirect('login')

#define a functions to handle signup and submission.
def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            confirm = form.cleaned_data('password_confirmation')
            if password == confirm:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password", "the passwords do not match")

    else:
        form = SignupForm()
    return render(request, 'accounts/signup.html', {'form': form})

# def create_user
# #define a function to show the login form for an HTTP GET,
# # and try to create a new user for an HTTP POST.
# # If the password and password_confirmation do not match,
# # then the User should not be created and there should be
# # an error that reads "the passwords do not match"
# # If the account is created,
# # you should redirect them to the list of projects.
# #create_user
# #login
#     pass
