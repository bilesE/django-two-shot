from django.contrib import admin
from .models import Receipt, ExpenseCategory, Account

# Register your models here.

@admin.register(Receipt)
@admin.register(ExpenseCategory)
@admin.register(Account)

class ReceiptAdmin(admin.ModelAdmin):
    pass
